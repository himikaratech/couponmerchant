package technologies.angular.lazymerchant;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.ViewHolder> {

    private ArrayList<MainActivityCouponSnippet> m_CouponList;
    private Activity m_activity;
    static OnItemClickListener mItemClickListener;

    public MainActivityAdapter(Context context, ArrayList<MainActivityCouponSnippet> couponList) {
        m_activity = (Activity) context;
        this.m_CouponList = couponList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.coupon_list_layout, parent, false);
        return new ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (!m_CouponList.isEmpty()) {
            final String couponName = m_CouponList.get(position).m_couponName;
            Log.i("coupon Name", couponName);
            String couponShortDesc = m_CouponList.get(position).m_short_desc;
            String couponImageAddress = m_CouponList.get(position).m_image_address;
            boolean couponImageFlag = m_CouponList.get(position).m_image_flag;

            WindowManager wm = (WindowManager) m_activity.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;

            Picasso.with(m_activity)
                    .load(couponImageAddress)
                    .resize(width, 0)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.no_image)
                    .into(holder.m_couponImage);

            holder.m_couponNameTextView.setText("Coupon Name: " + couponName);
            holder.m_couponShortDescTextView.setText("Description: " +couponShortDesc);
        }
    }


    @Override
    public int getItemCount() {
        return m_CouponList.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView m_couponNameTextView;
        TextView m_couponShortDescTextView;
        ImageView m_couponImage;

        public ViewHolder(View itemView, int flag) {
            super(itemView);

            m_couponNameTextView = (TextView) itemView.findViewById(R.id.coupon_name);
            m_couponShortDescTextView = (TextView) itemView.findViewById(R.id.coupon_short_desc);
            m_couponImage = (ImageView) itemView.findViewById(R.id.coupon_image);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public Object getItem(int position) {
        if (m_CouponList != null) {
            return m_CouponList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_CouponList != null) {
            return position;
        }
        return 0;
    }
}