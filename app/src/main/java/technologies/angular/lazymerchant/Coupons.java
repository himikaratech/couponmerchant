package technologies.angular.lazymerchant;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazymerchant.LocalDb.LazyMerchantContract;
import technologies.angular.lazymerchant.Network.ApiRequestModel;
import technologies.angular.lazymerchant.Network.ApiResponseModel;
import technologies.angular.lazymerchant.Network.ApiSuggestionService;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class Coupons extends Fragment {

    private ImageView m_couponImageView;
    private TextView m_couponNameTV;
    private TextView m_shortDescTV;
    private TextView m_descTV;
    private TextView m_termTV;
    private CouponDetailSnippet couponDetailSnippet;
    String merchant_code;
    private String coupon_name;
    private String coupon_type;

    public static Coupons getCouponName(String couponName,String couponType) {
        Coupons fragmentFirst = new Coupons();
        Bundle args = new Bundle();
        args.putString("coupon_name", couponName);
        args.putString("coupon_type", couponType);
        Log.d("couponType",couponType);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.coupon, container, false);

        coupon_name = getArguments().getString("coupon_name");
        coupon_type = getArguments().getString("coupon_type");

        Log.i("coupon name", coupon_name);

        m_couponImageView = (ImageView) v.findViewById(R.id.coupon_image);
        m_couponNameTV = (TextView) v.findViewById(R.id.coupon_name);
        m_shortDescTV = (TextView) v.findViewById(R.id.short_desc);
        m_descTV = (TextView) v.findViewById(R.id.desc);
        m_termTV = (TextView)v.findViewById(R.id.terms);

        m_couponNameTV.setVisibility(View.GONE);
        m_shortDescTV.setVisibility(View.GONE);
        m_descTV.setVisibility(View.GONE);
        m_couponImageView.setVisibility(View.GONE);
        m_termTV.setVisibility(View.GONE);

        couponDetailSnippet = new CouponDetailSnippet();

        loadCouponDetail(coupon_name,coupon_type);

        return v;
    }

    private void loadCouponDetail(String couponName,String coupon_type) {

        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyMerchantContract.MerchantDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    merchant_code = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if(m_cursor != null)m_cursor.close();

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);

        ApiRequestModel requestModel = new ApiRequestModel();
        final ApiRequestModel.LoadCouponAllDetailRequestModel loadCouponAllDetailRequestModel = requestModel.new LoadCouponAllDetailRequestModel();
        loadCouponAllDetailRequestModel.merchant_code = merchant_code;
        loadCouponAllDetailRequestModel.coupon_name = couponName;
        loadCouponAllDetailRequestModel.coupon_type=coupon_type;

        apiSuggestionsService.loadCouponAllDetailAPICall(loadCouponAllDetailRequestModel, new retrofit.Callback<ApiResponseModel.LoadCouponAllDetailResponseModel>() {

            @Override
            public void success(ApiResponseModel.LoadCouponAllDetailResponseModel loadCouponAllDetailResponseModel, retrofit.client.Response response) {

                boolean success = loadCouponAllDetailResponseModel.success;
                if (success == true) {
                    couponDetailSnippet = loadCouponAllDetailResponseModel.coupon_details;
                    String coupon_name = couponDetailSnippet.m_couponName;
                    String short_desc = couponDetailSnippet.m_short_desc;
                    String desc = couponDetailSnippet.m_desc;
                    String term = couponDetailSnippet.m_terms;

                   // boolean image_flag = couponDetailSnippet.m_coupon_img_flag;
                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int width = size.x;

                   /* if (image_flag != false) {
                        String image_address = couponDetailSnippet.m_coupon_img_add;
                        {

                            if (image_address == null || image_address == "")
                                image_address = "\"\"";
                            Picasso.with(getActivity())
                                    .load(image_address)
                                    .resize(width, 0)
                                    .placeholder(R.drawable.loading)
                                    .error(R.drawable.no_image)
                                    .into(m_couponImageView);

                            m_couponImageView.setVisibility(View.VISIBLE);
                        }
                    }*/

                    m_termTV.setText("Terms: " + term);
                    m_couponNameTV.setText("Coupon Name: " + coupon_name);
                    m_shortDescTV.setText(short_desc);
                    m_descTV.setText("Description: " + desc);
                    m_couponNameTV.setVisibility(View.VISIBLE);
                    m_shortDescTV.setVisibility(View.VISIBLE);
                    m_descTV.setVisibility(View.VISIBLE);
                    m_termTV.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }
}
