package technologies.angular.lazymerchant;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazymerchant.LocalDb.LazyMerchantContract;
import technologies.angular.lazymerchant.Network.ApiRequestModel;
import technologies.angular.lazymerchant.Network.ApiResponseModel;
import technologies.angular.lazymerchant.Network.ApiSuggestionService;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    private ArrayList<MainActivityCouponSnippet> m_CouponList;
    private MainActivityAdapter mainActivityAdapter;
    private String merchantCode;

    String TITLES[] = {"Check Coupon"};
    int ICONS[] = {R.mipmap.ic_launcher};

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout
    String Name;
    public static String mAddressOutput = "Coupon Merchant";
    public ActionBarDrawerToggle mDrawerToggle;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Cursor m_cursor = getContentResolver().query(
                LazyMerchantContract.MerchantDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    merchantCode = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView); // Assigning the RecyclerView Object to the xml View
        // mRecyclerView.setHasFixedSize(true);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayViewWidth_ = size.x;

        mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, MainActivity.this, 1);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer object Assigned to the view

        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.width = 9 * (displayViewWidth_ / 10);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutParams(params);

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.opendrawer, R.string.closedrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State


        recyclerView = (RecyclerView)findViewById(R.id.coupon_all_list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        loadCoupon(merchantCode);
    }

    private void loadCoupon(String m_merchantCode)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);

        ApiRequestModel requestModel=new ApiRequestModel();
        final ApiRequestModel.GetCouponRequestModel getAllCouponRequestModel=requestModel.new GetCouponRequestModel();
        getAllCouponRequestModel.merchant_code =m_merchantCode;

        apiSuggestionsService.getCouponAPICall(getAllCouponRequestModel, new retrofit.Callback<ApiResponseModel.GetCouponResponseModel>() {

            @Override
            public void success(final ApiResponseModel.GetCouponResponseModel getCouponResponseModel, retrofit.client.Response response) {

                if(getCouponResponseModel.success == true) {
                    m_CouponList = getCouponResponseModel.coupons;
                    mainActivityAdapter = new MainActivityAdapter(MainActivity.this, m_CouponList);
                    recyclerView.setAdapter(mainActivityAdapter);
                    mainActivityAdapter.SetOnItemClickListener(new MainActivityAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position) {
                            Intent intent = new Intent(MainActivity.this, MainActivityCouponDetails.class);
                            intent.putExtra("coupon_name", getCouponResponseModel.coupons.get(position).m_couponName);
                            intent.putExtra("coupon_type", getCouponResponseModel.coupons.get(position).m_couponType);
                            intent.putExtra("redirect_type", getCouponResponseModel.coupons.get(position).m_redirectType);
                            startActivity(intent);
                        }
                    });
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();

            }
        });
    }

    @Override
    public void  onResume()
    {
        super.onResume();
    }
}
