package technologies.angular.lazymerchant;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazymerchant.LocalDb.LazyMerchantContract;
import technologies.angular.lazymerchant.Network.ApiRequestModel;
import technologies.angular.lazymerchant.Network.ApiResponseModel;
import technologies.angular.lazymerchant.Network.ApiSuggestionService;

/**
 * Created by Amud on 04/03/16.
 */
public class ItemStatics extends AppCompatActivity {



        TextView nameTextView;
        TextView viewsTextView;
        TextView hitsTextView;
        TextView conversionTextView;
        TextView numberOfCouponsTextView;
       TextView couponRedeemedTextView;
    View couponRedeemedView;


        int view, hit, convert, number;
        String name;
        String merchant_code;
        String coupon_name;



        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.analytics);



            name = getIntent().getStringExtra("item_name");
            Log.d("name",name);
            view = getIntent().getIntExtra("views",0);
            hit = getIntent().getIntExtra("clicks",0);
            convert = getIntent().getIntExtra("conversions",0);
            number = getIntent().getIntExtra("num_coupons",0);

            nameTextView = (TextView)findViewById(R.id.name_textView);
            viewsTextView = (TextView)findViewById(R.id.views_textView);
            hitsTextView = (TextView)findViewById(R.id.hits_textView);
            conversionTextView = (TextView)findViewById(R.id.conversion_textView);
            numberOfCouponsTextView = (TextView)findViewById(R.id.no_of_coupon_textView);
            couponRedeemedTextView = (TextView)findViewById(R.id.coupons_redeemed_textView);
            couponRedeemedView=findViewById(R.id.coupons_redeemed);

            couponRedeemedView.setVisibility(View.GONE);

            couponRedeemedTextView.setVisibility(View.GONE);


            getAnalyticsDetails();

        }

        private void getAnalyticsDetails() {




                        nameTextView.setText(name);
                        viewsTextView.setText(""+view);
                        hitsTextView.setText(""+hit);
                        numberOfCouponsTextView.setText(""+number);
                        conversionTextView.setText(""+convert);
                      //  couponRedeemedTextView.setText(""+redeemed);


        }


}
