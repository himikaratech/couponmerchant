package technologies.angular.lazymerchant;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Amud on 25/01/16.
 */
public class CouponDetailSnippet {

    @SerializedName("coupon_name")
    public String m_couponName;
    @SerializedName("number_of_coupons")
    public int m_noOfCoupons;
    @SerializedName("coupon_desc")
    public String m_desc;
    @SerializedName("coupon_short_desc")
    public String m_short_desc;
    @SerializedName("terms")
    String m_terms;
   /* @SerializedName("img_flag")
    public boolean m_coupon_img_flag;*/
    @SerializedName("img_address")
    public  String  m_coupon_img_add;

    public CouponDetailSnippet(){
    }

    public CouponDetailSnippet(String couponName, int noOfCoupons, String desc, String short_desc, String terms,
                               boolean coupon_img_flag, String coupon_img_add){
        m_couponName=couponName;
        m_noOfCoupons = noOfCoupons;
        m_desc=desc;
        m_short_desc=short_desc;
        m_terms=terms;
/*
        m_coupon_img_flag=coupon_img_flag;
*/
        m_coupon_img_add=coupon_img_add;
    }
}

