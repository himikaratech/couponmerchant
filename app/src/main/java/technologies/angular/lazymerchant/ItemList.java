package technologies.angular.lazymerchant;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Vector;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazymerchant.Adapters.ItemsListAdapter;
import technologies.angular.lazymerchant.LocalDb.LazyMerchantContract;
import technologies.angular.lazymerchant.Network.ApiResponseModel;
import technologies.angular.lazymerchant.Network.ApiSuggestionService;
import technologies.angular.lazymerchant.Snippets.CouponAnalyticsSnippet;

/**
 * Created by Amud on 04/03/16.
 */
public class ItemList extends AppCompatActivity {
    private ListView m_itemDetailsListView;
    private ArrayList<CouponAnalyticsSnippet> m_itemsList = null;
    private ItemsListAdapter m_itemsDetailAdapter;
    private String m_couponName;
    private String m_merchantCode;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_list);
        m_couponName=getIntent().getStringExtra("coupon_name");
        Log.d("coupon_name",m_couponName);


        Cursor m_cursor = getContentResolver().query(
                LazyMerchantContract.MerchantDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_merchantCode = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }


            m_itemDetailsListView = (ListView) findViewById(R.id.items_fg_details_list_view);




        getItemsFromServerLimited();

        }




        public void getItemsFromServerLimited() {


            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.show();

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
            apiSuggestionsService.itemCouponStaticsAPICall(m_couponName,m_merchantCode, new retrofit.Callback<ApiResponseModel.ItemCouponAnalyticsResponseModel>() {

                @Override
                public void success(ApiResponseModel.ItemCouponAnalyticsResponseModel itemCouponAnalyticsResponseModel, retrofit.client.Response response) {
                    ArrayList<CouponAnalyticsSnippet> itemsList = new ArrayList<>();

                    itemsList = itemCouponAnalyticsResponseModel.coupon_analytics;
                    if(itemsList!=null ) {
                        m_itemsDetailAdapter = new ItemsListAdapter(ItemList.this, itemsList);
                        m_itemDetailsListView.setAdapter(m_itemsDetailAdapter);


                    }
                    if(progressDialog.isShowing())
                        progressDialog.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("retro", error + "");
                    progressDialog.dismiss();
                }
            });
        }


    }


