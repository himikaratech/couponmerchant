package technologies.angular.lazymerchant.Network;

import java.util.ArrayList;

import technologies.angular.lazymerchant.AnalyticSnippet;
import technologies.angular.lazymerchant.CouponDetailSnippet;
import technologies.angular.lazymerchant.MainActivityCouponSnippet;
import technologies.angular.lazymerchant.Snippets.AnalyticLazyladSnippet;
import technologies.angular.lazymerchant.Snippets.CouponAnalyticsSnippet;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class ApiResponseModel {

    public class ResponseDataLogin {
        public boolean success;
        public String merchant_code;
        public boolean merchant_authenticated;
        public boolean merchant_exist;
        public String merchant_name;
    }

    public class GetCouponResponseModel{
        public boolean success;
        public ArrayList<MainActivityCouponSnippet> coupons;
    }

    public class AnalyticResponse {
        public boolean success;
        public AnalyticSnippet coupon_analytics;
    }

    public class AnalyticLazyladResponse {
        public boolean success;
        public AnalyticLazyladSnippet coupon_analytics;
    }

    public class LoadCouponAllDetailResponseModel {
        public boolean success;
        public CouponDetailSnippet coupon_details;
    }

    public class VerifyCouponResponseModel {
        public boolean success;
        public boolean coupon_valid;
    }

    public class  ItemCouponAnalyticsResponseModel{
        public boolean success;
        public ArrayList<CouponAnalyticsSnippet> coupon_analytics;

    }
}
