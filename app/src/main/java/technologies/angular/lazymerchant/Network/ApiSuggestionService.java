package technologies.angular.lazymerchant.Network;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by sakshigupta on 29/01/16.
 */
public interface ApiSuggestionService {

    @POST("/newserver/lazyad/merchant/sign_in_merchant")
    void login(@Body ApiRequestModel.User user, Callback<ApiResponseModel.ResponseDataLogin> callback);

    @POST("/newserver/lazyad/merchant/get_coupons")
    void getCouponAPICall(@Body ApiRequestModel.GetCouponRequestModel param,
                             Callback<ApiResponseModel.GetCouponResponseModel> cb);

    @POST("/newserver/lazyad/merchant/coupon_analytics")
    void getAnalyticsDataAPICall(@Body ApiRequestModel.Analytics param,
                          Callback<ApiResponseModel.AnalyticResponse> cb);


    @GET("/newserver/lazyad/merchant/lazylad_coupon_analytics")
    void getLazyladAnalyticsDataAPICall(@Query("merchant_code") String merchant_code,
                                        @Query("coupon_name") String  coupon_name,
                                 Callback<ApiResponseModel.AnalyticLazyladResponse> cb);

    @POST("/newserver/lazyad/merchant/coupon_details")
    void loadCouponAllDetailAPICall(@Body ApiRequestModel.LoadCouponAllDetailRequestModel param,
                                    Callback<ApiResponseModel.LoadCouponAllDetailResponseModel> cb);

    @POST("/newserver/lazyad/merchant/coupon_verification")
    void verifyCoupon(@Body ApiRequestModel.VerifyCouponRequestModel param,
                                    Callback<ApiResponseModel.VerifyCouponResponseModel> cb);



    @GET("/newserver/lazyad/merchant/lazylad_item_coupon_analytics")
    void itemCouponStaticsAPICall(@Query("coupon_name") String coupon_name,
                        @Query("merchant_code") String merchant_code,
                        Callback<ApiResponseModel.ItemCouponAnalyticsResponseModel> cb);
}
