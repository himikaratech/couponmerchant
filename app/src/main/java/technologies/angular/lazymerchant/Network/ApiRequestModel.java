package technologies.angular.lazymerchant.Network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class ApiRequestModel {

    public class User {
        @SerializedName("phone_number")
        public String username;
        public String password;
    }

    public class GetCouponRequestModel{
        public String merchant_code;
    }

    public class Analytics {
        public String merchant_code;
        public String coupon_name;
    }

    public class LoadCouponAllDetailRequestModel {
        public  String merchant_code;
        public String coupon_name;
        public String coupon_type;
    }

    public class VerifyCouponRequestModel {
        public  String merchant_code;
        public String coupon_code;
    }
}
