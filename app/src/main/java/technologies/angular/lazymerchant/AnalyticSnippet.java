package technologies.angular.lazymerchant;

/**
 * Created by sakshigupta on 01/02/16.
 */
public class AnalyticSnippet {

    public String name;
    public int hits;
    public int views;
    public int conversions;
    public int number_of_coupons;
    public int coupons_redeemed;
    public String coupon_desc;
    public String coupon_short_desc;
    public String terms;
    public boolean img_flag;
    public String img_address;

    public AnalyticSnippet() {
    }

    public AnalyticSnippet(String couponName, int hit, int view, int conversion, int noOfCoupons,
                           int couponRedeemed, String couponDesc, String couponShortDesc, String term,
                           boolean flag, String address) {
        name = couponName;
        hits = hit;
        views = view;
        conversions = conversion;
        number_of_coupons = noOfCoupons;
        coupons_redeemed = couponRedeemed;
        coupon_desc = couponDesc;
        coupon_short_desc = couponShortDesc;
        terms = term;
        img_flag = flag;
        img_address = address;
    }
}
