package technologies.angular.lazymerchant;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazymerchant.LocalDb.LazyMerchantContract;
import technologies.angular.lazymerchant.Network.ApiRequestModel;
import technologies.angular.lazymerchant.Network.ApiResponseModel;
import technologies.angular.lazymerchant.Network.ApiSuggestionService;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class Analytics extends Fragment {

    TextView names;
    TextView views;
    TextView hits;
    TextView conversion;
    TextView numberOfCoupons;
    TextView couponRedeemed;
    TextView nameTextView;
    TextView viewsTextView;
    TextView hitsTextView;
    TextView conversionTextView;
    TextView numberOfCouponsTextView;
    TextView couponRedeemedTextView;
    RelativeLayout name_layout;
    RelativeLayout views_layout;
    RelativeLayout hits_layout;
    RelativeLayout conversion_layout;
    RelativeLayout no_of_coupons_layout;
    RelativeLayout coupon_redeemed_layout;

    int view, hit, convert, number, redeemed;
    String name;
    String merchant_code;
    String coupon_name;
    private String coupon_type;

    public static Analytics getCouponName(String couponName, String couponType) {
        Analytics fragmentFirst = new Analytics();
        Bundle args = new Bundle();
        args.putString("coupon_name", couponName);
        args.putString("coupon_type", couponType);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.analytics, container, false);

        coupon_name = getArguments().getString("coupon_name");
        coupon_type = getArguments().getString("coupon_type");

        Log.i("coupon analytics", coupon_name);
        nameTextView = (TextView) v.findViewById(R.id.name_textView);
        viewsTextView = (TextView) v.findViewById(R.id.views_textView);
        hitsTextView = (TextView) v.findViewById(R.id.hits_textView);
        conversionTextView = (TextView) v.findViewById(R.id.conversion_textView);
        numberOfCouponsTextView = (TextView) v.findViewById(R.id.no_of_coupon_textView);
        couponRedeemedTextView = (TextView) v.findViewById(R.id.coupons_redeemed_textView);

        names = (TextView) v.findViewById(R.id.name);
        views = (TextView) v.findViewById(R.id.views);
        hits = (TextView) v.findViewById(R.id.hits);
        conversion = (TextView) v.findViewById(R.id.conversion);
        numberOfCoupons = (TextView) v.findViewById(R.id.no_of_coupon);
        couponRedeemed = (TextView) v.findViewById(R.id.coupons_redeemed);

        name_layout = (RelativeLayout) v.findViewById(R.id.name_layout);
        views_layout = (RelativeLayout) v.findViewById(R.id.views_layout);
        hits_layout = (RelativeLayout) v.findViewById(R.id.hits_layout);
        conversion_layout = (RelativeLayout) v.findViewById(R.id.conversion_layout);
        no_of_coupons_layout = (RelativeLayout) v.findViewById(R.id.no_of_coupon_layout);
        coupon_redeemed_layout = (RelativeLayout) v.findViewById(R.id.coupons_redeemed_layout);

        name_layout.setVisibility(View.GONE);
        views_layout.setVisibility(View.GONE);
        hits_layout.setVisibility(View.GONE);
        conversion_layout.setVisibility(View.GONE);
        no_of_coupons_layout.setVisibility(View.GONE);
        coupon_redeemed_layout.setVisibility(View.GONE);



        if (coupon_type != null && coupon_type.equals("lazylad"))
            getAnalyticsLazyladDetails();
        else
            getAnalyticsDetails();
        return v;
    }

    private void getAnalyticsDetails() {

        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyMerchantContract.MerchantDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    merchant_code = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);

        ApiRequestModel requestModel = new ApiRequestModel();
        final ApiRequestModel.Analytics analyticsRequestModel = requestModel.new Analytics();
        analyticsRequestModel.merchant_code = merchant_code;
        analyticsRequestModel.coupon_name = coupon_name;

        apiSuggestionsService.getAnalyticsDataAPICall(analyticsRequestModel, new retrofit.Callback<ApiResponseModel.AnalyticResponse>() {

            @Override
            public void success(ApiResponseModel.AnalyticResponse output, retrofit.client.Response response) {
                boolean success = output.success;
                if (success == true) {
                    name = output.coupon_analytics.name;
                    view = output.coupon_analytics.views;
                    hit = output.coupon_analytics.hits;
                    convert = output.coupon_analytics.conversions;
                    number = output.coupon_analytics.number_of_coupons;
                    redeemed = output.coupon_analytics.coupons_redeemed;

                    Log.i("views", view + "");
                    name_layout.setVisibility(View.VISIBLE);
                    views_layout.setVisibility(View.VISIBLE);
                    hits_layout.setVisibility(View.VISIBLE);
                    no_of_coupons_layout.setVisibility(View.VISIBLE);
                    coupon_redeemed_layout.setVisibility(View.VISIBLE);

                    nameTextView.setText(name);
                    viewsTextView.setText("" + view);
                    hitsTextView.setText("" + hit);
                    numberOfCouponsTextView.setText("" + number);
                    conversionTextView.setText("" + convert);
                    couponRedeemedTextView.setText("" + redeemed);

                    if (pDialog.isShowing())
                        pDialog.dismiss();
                }
            }

            @Override
            public void failure(final RetrofitError error) {
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }
        });
    }




    private void getAnalyticsLazyladDetails() {

        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyMerchantContract.MerchantDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    merchant_code = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }
        if (m_cursor != null) m_cursor.close();

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);

        ApiRequestModel requestModel = new ApiRequestModel();


        apiSuggestionsService.getLazyladAnalyticsDataAPICall(merchant_code, coupon_name, new retrofit.Callback<ApiResponseModel.AnalyticLazyladResponse>() {

            @Override
            public void success(ApiResponseModel.AnalyticLazyladResponse output, retrofit.client.Response response) {
                boolean success = output.success;
                if (success == true) {
                    name = output.coupon_analytics.coupon_name;
                    view = output.coupon_analytics.views;
                    hit = output.coupon_analytics.clicks;
                    //  convert = output.coupon_analytics.conversions;
                    number = output.coupon_analytics.num_coupons;
                    redeemed = output.coupon_analytics.num_coupons_used;

                    Log.i("views", view + "");
                    name_layout.setVisibility(View.VISIBLE);
                    views_layout.setVisibility(View.VISIBLE);
                    hits_layout.setVisibility(View.VISIBLE);
                    conversion_layout.setVisibility(View.VISIBLE);
                    no_of_coupons_layout.setVisibility(View.VISIBLE);
                    coupon_redeemed_layout.setVisibility(View.VISIBLE);

                    nameTextView.setText(name);
                    viewsTextView.setText("" + view);
                    hitsTextView.setText("" + hit);
                    numberOfCouponsTextView.setText("" + number);
                    conversionTextView.setVisibility(View.GONE);

                    couponRedeemedTextView.setText("" + redeemed);

                    if (pDialog.isShowing())
                        pDialog.dismiss();
                }
            }

            @Override
            public void failure(final RetrofitError error) {
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }
        });
    }
}
