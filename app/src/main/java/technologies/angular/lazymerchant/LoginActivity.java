package technologies.angular.lazymerchant;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazymerchant.LocalDb.LazyMerchantContract;
import technologies.angular.lazymerchant.Network.ApiRequestModel;
import technologies.angular.lazymerchant.Network.ApiResponseModel;
import technologies.angular.lazymerchant.Network.ApiSuggestionService;

/**
 * Created by sakshigupta on 29/01/16.
 */

public class LoginActivity extends Activity {

    private EditText m_emailEditView;
    private EditText m_passwordEditView;

    private String m_email;
    private String m_password;

    private Button m_signInButton;

    private String merchantCode;
    private String merchantName;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        m_emailEditView = (EditText) findViewById(R.id.email_editview);
        m_passwordEditView = (EditText) findViewById(R.id.password_editview);
        m_signInButton = (Button) findViewById(R.id.sign_in_button);
        merchantCode = "0";
        context = getApplicationContext();

        m_signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_email = m_emailEditView.getText().toString();
                m_password = m_passwordEditView.getText().toString();
                if (m_email.isEmpty() || m_email == null) {
                    Toast.makeText(getApplicationContext(), "Please enter your username", Toast.LENGTH_SHORT).show();
                } else if (m_password.isEmpty() || m_password == null) {
                    Toast.makeText(getApplicationContext(), "Please enter your password", Toast.LENGTH_SHORT).show();
                } else {
                    SignInServProv();
                }
            }
        });
    }

    private void SignInServProv() {
        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Signing In. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        ApiRequestModel requestModel = new ApiRequestModel();
        final ApiRequestModel.User loginModel = requestModel.new User();
        loginModel.username = m_email;
        loginModel.password = m_password;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);
        apiSuggestionsService.login(loginModel, new Callback<ApiResponseModel.ResponseDataLogin>() {
            @Override
            public void success(ApiResponseModel.ResponseDataLogin responseLogin, Response response) {
                boolean success = responseLogin.success;
                Log.i("success", success+"");

                if (success == true) {
                    merchantCode = responseLogin.merchant_code;
                    merchantName = responseLogin.merchant_name;
                    Log.i("merchant name", responseLogin.merchant_name);
                    ContentValues values = new ContentValues();
                    values.put(LazyMerchantContract.MerchantDetails.COLUMN_MERCHANT_CODE, merchantCode);
                    values.put(LazyMerchantContract.MerchantDetails.COLUMN_MERCHANT_USERNAME, m_email);
                    values.put(LazyMerchantContract.MerchantDetails.COLUMN_MERCHANT_PASSWORD, m_password);
                    values.put(LazyMerchantContract.MerchantDetails.COLUMN_MERCHANT_NAME, merchantName);

                    getContentResolver().insert(LazyMerchantContract.MerchantDetails.CONTENT_URI, values);

                } else {
                    Toast.makeText(LoginActivity.this, "Invalid Login Details", Toast.LENGTH_SHORT).show();
                }

                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                pDialog.dismiss();
            }

            @Override
            public void failure(final RetrofitError error) {
                Log.i("failure", error.getMessage());
                pDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
