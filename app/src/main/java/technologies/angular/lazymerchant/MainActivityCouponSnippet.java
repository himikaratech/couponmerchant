package technologies.angular.lazymerchant;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class MainActivityCouponSnippet {

    @SerializedName("coupon_name")
    public String m_couponName;
    @SerializedName("img_flag")
    public boolean m_image_flag;
    @SerializedName("img_address")
    public String m_image_address;
    @SerializedName("coupon_short_desc")
    public String m_short_desc;
    @SerializedName("coupon_type")
    public String m_couponType;
    @SerializedName("redirect_type")
    public String m_redirectType;

    public MainActivityCouponSnippet(){
    }

    public MainActivityCouponSnippet(String couponName , String short_desc , boolean image_flag, String image_address){
        m_couponName=couponName;
        m_short_desc=short_desc;
        m_image_flag=image_flag;
        m_image_address=image_address;
    }
}
