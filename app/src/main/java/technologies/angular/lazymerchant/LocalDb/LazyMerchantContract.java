package technologies.angular.lazymerchant.LocalDb;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class LazyMerchantContract {

    public static final String CONTENT_AUTHORITY = "technologies.angular.lazymerchant.provider";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_MERCHANT_DETAILS = "merchant_details";

    /* Inner class that defines the table contents of the userDetails table */
    public static final class MerchantDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MERCHANT_DETAILS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_MERCHANT_DETAILS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_MERCHANT_DETAILS;

        // Table name
        public static final String TABLE_NAME = "merchant_details";

        public static final String COLUMN_MERCHANT_CODE = "merchant_code";
        public static final String COLUMN_MERCHANT_USERNAME = "merchant_username";
        public static final String COLUMN_MERCHANT_PASSWORD = "merchant_passowrd";
        public static final String COLUMN_MERCHANT_NAME = "merchant_name";

        public static Uri buildUserDetailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
