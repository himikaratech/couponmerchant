package technologies.angular.lazymerchant.LocalDb;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class LazyMerchantProvider extends ContentProvider{
    private static UriMatcher sUriMatcher  = buildUriMatcher();

    private DbHelper mOpenHelper;

    private static final int MERCHANT_DETAILS = 100;

    private static final SQLiteQueryBuilder sLazyLadServiceProviderQueryBuilder;

    static{
        sLazyLadServiceProviderQueryBuilder = new SQLiteQueryBuilder();
        sLazyLadServiceProviderQueryBuilder.setTables(LazyMerchantContract.MerchantDetails.TABLE_NAME );
    }


    private static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = LazyMerchantContract.CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        matcher.addURI(authority, LazyMerchantContract.PATH_MERCHANT_DETAILS, MERCHANT_DETAILS);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DbHelper(getContext());
        return true;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {

            case MERCHANT_DETAILS:
            {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        LazyMerchantContract.MerchantDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case MERCHANT_DETAILS:
                return LazyMerchantContract.MerchantDetails.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case MERCHANT_DETAILS: {
                long _id = db.insert(LazyMerchantContract.MerchantDetails.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = LazyMerchantContract.MerchantDetails.buildUserDetailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        switch (match) {
            case MERCHANT_DETAILS: {
                rowsDeleted = db.delete(
                        LazyMerchantContract.MerchantDetails.TABLE_NAME, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (selection == null || rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }


    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case MERCHANT_DETAILS: {
                rowsUpdated = db.update(
                        LazyMerchantContract.MerchantDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }
}
