package technologies.angular.lazymerchant.LocalDb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static int DATABASE_VERSION = 1;

    private static String DATABASE_NAME = "LazyLadMerchantData";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_MERCHANT_DETAILS_TABLE = "CREATE TABLE IF NOT EXISTS " + LazyMerchantContract.MerchantDetails.TABLE_NAME + "(" +
                LazyMerchantContract.MerchantDetails._ID + " INTEGER PRIMARY KEY, " +
                LazyMerchantContract.MerchantDetails.COLUMN_MERCHANT_CODE + " TEXT, " +
                LazyMerchantContract.MerchantDetails.COLUMN_MERCHANT_USERNAME + " TEXT, " +
                LazyMerchantContract.MerchantDetails.COLUMN_MERCHANT_PASSWORD + " TEXT, " +
                LazyMerchantContract.MerchantDetails.COLUMN_MERCHANT_NAME + " TEXT " +
                " );";

        db.execSQL(SQL_CREATE_MERCHANT_DETAILS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + LazyMerchantContract.MerchantDetails.TABLE_NAME);
        onCreate(db);
    }
}
