package technologies.angular.lazymerchant.Adapters;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import technologies.angular.lazymerchant.ItemList;
import technologies.angular.lazymerchant.ItemStatics;
import technologies.angular.lazymerchant.Network.ApiResponseModel;
import technologies.angular.lazymerchant.R;
import technologies.angular.lazymerchant.Snippets.CouponAnalyticsSnippet;

/**
 * Created by Amud on 04/03/16.
 */

public class ItemsListAdapter extends BaseAdapter {
    public ArrayList<CouponAnalyticsSnippet> m_itemsDetailsAdapterList;
    private Context m_activity;


    private class ViewHolder {

        TextView itemNameTextView ;

        TextView Description ;
        ImageView imageView ;
        View m_itemView;



    }

    public ItemsListAdapter(Context activity, ArrayList<CouponAnalyticsSnippet> itemsDetails) {
        m_activity = activity;
        m_itemsDetailsAdapterList = itemsDetails;
    }



    @Override
    public int getCount() {
        int count = 0;
        if (m_itemsDetailsAdapterList != null) {
            count = m_itemsDetailsAdapterList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_itemsDetailsAdapterList != null) {
            return m_itemsDetailsAdapterList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_itemsDetailsAdapterList != null) {
            return position;
        }
        return 0;
    }

    /*@Override
    public long getItemId(int position) {
        if (m_itemsDetailsAdapterList != null) {
            return m_itemsDetailsAdapterList.get(position).;
        }
        return 0;
    }*/



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        if (m_itemsDetailsAdapterList != null) {

            final ViewHolder holder;


            final String itemName = ((CouponAnalyticsSnippet) getItem(position)).item_name;
            int itemImageFlag = ((CouponAnalyticsSnippet) getItem(position)).item_img_flag;
            String itemImageAdd = ((CouponAnalyticsSnippet) getItem(position)).item_img_add;
            final String itemDesc = ((CouponAnalyticsSnippet) getItem(position)).item_desc;




            final int itemPosition = position;


            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    convertView = infalInflater.inflate(R.layout.item_display_list, (ViewGroup) null);
                holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name);
                holder.Description = (TextView) convertView.findViewById(R.id.item_description);
                holder.m_itemView=convertView.findViewById(R.id.item_view);
                holder.imageView=(ImageView)convertView.findViewById(R.id.item_image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
                   }




            holder.m_itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(m_activity, ItemStatics.class);

                    Log.d("item_name",((CouponAnalyticsSnippet) getItem(itemPosition)).item_name);

                                intent.putExtra("item_name", ((CouponAnalyticsSnippet) getItem(itemPosition)).item_name);
                                intent.putExtra("views", ((CouponAnalyticsSnippet) getItem(itemPosition)).views);
                                intent.putExtra("clicks", ((CouponAnalyticsSnippet) getItem(itemPosition)).clicks);
                    intent.putExtra("conversions", ((CouponAnalyticsSnippet) getItem(itemPosition)).conversions);
                    intent.putExtra("num_coupons", ((CouponAnalyticsSnippet) getItem(itemPosition)).num_coupons);

                    m_activity.startActivity(intent);
                }
            });


            holder.Description.setText(itemDesc);

            if (itemImageFlag==1) {


                if (itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
                Picasso.with(parent.getContext())
                        .load(itemImageAdd)
                        .fit().centerInside().
                        placeholder(R.drawable.loading)
                        .error(R.drawable.no_image)
                        .into(holder.imageView);
            }


            holder.itemNameTextView.setText(itemName);






        }

        return convertView;
    }





}
