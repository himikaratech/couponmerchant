package technologies.angular.lazymerchant;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class MainActivityCouponDetails extends AppCompatActivity {

    private ViewPager m_viewPager;
    private Toolbar m_toolbar;
    private MainActivityCouponPagerAdapter couponPagerAdapter ;
    CharSequence Titles[]={"Analytics","Coupons"};
    int Numboftabs =2;

    String TITLES[] = {"Check Coupon"};
    int ICONS[] = {R.mipmap.ic_launcher};

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout
    String Name;
    public static String mAddressOutput = "Coupon Merchant";
    public ActionBarDrawerToggle mDrawerToggle;
    Toolbar toolbar;
    String coupon_name;
    String m_couponType;
    String m_redirectType;
    private Button m_itemStatics;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_layout_for_coupon);
        m_toolbar = (Toolbar) findViewById(R.id.coupon_toolbar);
        m_itemStatics=(Button)findViewById(R.id.view_item_statics);
        setSupportActionBar(m_toolbar);

        m_itemStatics.setVisibility(View.GONE);

        Intent intent = getIntent();
        coupon_name = intent.getExtras().getString("coupon_name");
        m_couponType = intent.getExtras().getString("coupon_type");
        m_redirectType= intent.getExtras().getString("redirect_type");



        if(m_redirectType !=null && m_redirectType.equals("item"))
        {
            m_itemStatics.setVisibility(View.VISIBLE);
            m_itemStatics.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(MainActivityCouponDetails.this, ItemList.class);
                    intent.putExtra("coupon_name",coupon_name) ;
                      startActivity(intent);

                }
            });
        }


        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView); // Assigning the RecyclerView Object to the xml View

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayViewWidth_ = size.x;

        mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, MainActivityCouponDetails.this, 1);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer object Assigned to the view

        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.width = 9 * (displayViewWidth_ / 10);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutParams(params);

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.opendrawer, R.string.closedrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State

        m_viewPager=(ViewPager)findViewById(R.id.coupon_pager);
        couponPagerAdapter= new MainActivityCouponPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs, coupon_name,m_couponType);
        m_viewPager.setAdapter(couponPagerAdapter);

    }
}
