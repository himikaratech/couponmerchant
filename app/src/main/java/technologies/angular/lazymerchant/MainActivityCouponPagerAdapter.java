package technologies.angular.lazymerchant;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by sakshigupta on 29/01/16.
 */
public class MainActivityCouponPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[];
    int NumbOfTabs;
    String coupon_name;
    String coupon_type;

    public MainActivityCouponPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, String couponName,String coupon_type) {
        super(fm);
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        coupon_name = couponName;
        this.coupon_type=coupon_type;
    }

    @Override
    public Fragment getItem(int position) {

        if(position == 0)         {
            Analytics analytics = new Analytics();
            return analytics.getCouponName(coupon_name,coupon_type);
        }
        else
        {
            Coupons coupons = new Coupons();
            return coupons.getCouponName(coupon_name,coupon_type);
        }
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
