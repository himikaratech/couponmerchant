package technologies.angular.lazymerchant;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazymerchant.LocalDb.LazyMerchantContract;
import technologies.angular.lazymerchant.Network.ApiRequestModel;
import technologies.angular.lazymerchant.Network.ApiResponseModel;
import technologies.angular.lazymerchant.Network.ApiSuggestionService;

/**
 * Created by sakshigupta on 01/02/16.
 */
public class CheckCoupon extends Activity {

    EditText verifyEditText;
    Button verifyButton;
    String verifyStr;
    private String merchantCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_coupon);
        verifyButton = (Button)findViewById(R.id.verify);
        verifyEditText = (EditText)findViewById(R.id.verify_coupon);

        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyStr = verifyEditText.getText().toString();
                if (verifyStr.isEmpty() || verifyStr == null) {
                    Toast.makeText(getApplicationContext(), "Please enter coupon code", Toast.LENGTH_SHORT).show();
                } else {
                    verifyCoupon();
                }
            }
        });
    }

    private void verifyCoupon() {
        Cursor m_cursor = getContentResolver().query(
                LazyMerchantContract.MerchantDetails.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    merchantCode = m_cursor.getString(1);
                    m_cursor.moveToNext();
                }
            } else {
                //TODO Some UI Element asking to add address
            }
        } else {
            //TODO Error Handling
        }

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        ApiSuggestionService apiSuggestionsService = restAdapter.create(ApiSuggestionService.class);

        ApiRequestModel requestModel=new ApiRequestModel();
        final ApiRequestModel.VerifyCouponRequestModel verifyCouponRequestModel=requestModel.new VerifyCouponRequestModel();
        verifyCouponRequestModel.merchant_code = merchantCode;
        verifyCouponRequestModel.coupon_code = verifyStr;
        apiSuggestionsService.verifyCoupon(verifyCouponRequestModel, new retrofit.Callback<ApiResponseModel.VerifyCouponResponseModel>() {

            @Override
            public void success(ApiResponseModel.VerifyCouponResponseModel verifyCouponResponseModel, retrofit.client.Response response) {

                if(verifyCouponResponseModel.success == true) {
                    boolean couponValid = verifyCouponResponseModel.coupon_valid;
                    verifyEditText.setText("");
                    Toast.makeText(CheckCoupon.this, "Successfully verified", Toast.LENGTH_SHORT).show();
                    Log.i("coupon validation", couponValid+"");
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }
}
